const bankBalanceEle=document.getElementById("bank-balance")
const applyBtnEle=document.getElementById("apply-btn")
const repayBtnEle=document.getElementById("repay-btn")
const loanBalanceEle=document.getElementById("loan-balance")
const balanceEle=document.getElementById("balance")

const workBtnEle=document.getElementById("work-btn")
const depositeBtnEle=document.getElementById("deposit-btn")
const selectLaptopEle=document.getElementById("select-laptop")
const imageEle=document.getElementById("laptop-image")

const descriptionElement = document.getElementById("laptop-description")
const specElement = document.getElementById("laptop-specs")
const buyBtnEle=document.getElementById("buy-btn")

const priceElement = document.getElementById("price");

let bankBalance = 0;
let payBalance = 0;
let loanBalance = 0;
let laptops = [];
let price = 200;
let noLaptop = true
let hasLent= false 

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data=> laptops = data)
    .then(laptops=> addLaptopsToMenu(laptops))

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => populateStore(x));
        //populate html with laptop 0

    priceElement.innerHTML = laptops[0].price;
    descriptionElement.innerText = laptops[0].description
    imageEle.innerHTML = `<img src="https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}" width="150" height="150" alt="Laptop image">`

    populateSpecs(laptops[0].specs)

}

const populateStore = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id 
    laptopElement.appendChild(document.createTextNode(laptop.title))
    selectLaptopEle.appendChild(laptopElement)
    priceElement.innerHTML
}

const handleLaptopMeneChange = e =>{
    const selectedLaptop = laptops[e.target.selectedIndex]
    priceElement.innerText = selectedLaptop.price
    price = selectedLaptop.price
    descriptionElement.innerText = selectedLaptop.description

    //clear previous ul with the specs
    document.getElementById("laptop-specs").innerHTML = "";

    populateSpecs(selectedLaptop.specs)

    imageEle.innerHTML = `<img src="https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}" width="150" height="150" alt="Laptop image">`
    
}

const populateSpecs = selectedLaptop => {
    selectedLaptop.forEach(laptop => {
        const temp = document.createElement('li')
        temp.innerText = laptop
        specElement.appendChild(temp)
    });
}

const handleWork = () =>{
    payBalance += 100
    balanceEle.innerText = `Pay balance: $${payBalance}`
}

const handleDeposit = () => {

    if (loanBalance>0){
        bankBalance += payBalance * 0.9
        loanBalance -= payBalance * 0.1
        loanBalanceEle.innerText = `Loan balance: $${loanBalance}`
        if (loanBalance<0){
            repayBtnEle.style.visibility = "hidden";
        }        
    }
    else{
    bankBalance += payBalance
    }
    payBalance=0
    balanceEle.innerText = `Pay balance: $${payBalance}`
    bankBalanceEle.innerText = `Bank balance: $${bankBalance}`
}   

const handleRepay = () => {
    const totalPaid = prompt("please enter the amount you wish to repay")
    const change = parseFloat(totalPaid)

    if(change>bankBalance){
        alert("You don't have that much money")
    }
    else if (change>loanBalance){
        alert("You  can't repay money you don't owe.")
    }
    else{
        loanBalance -= change
        bankBalance -= change
        loanBalanceEle.innerText = `Loan balance: $${loanBalance}`
        bankBalanceEle.innerText = `Bank balance: $${bankBalance}`
        if (loanBalance<=0){
            repayBtnEle.style.visibility = "hidden";
        }
    }
    
}

const handleBuy = () =>{
    console.log(`and your balance is  ${payBalance}`)
    if(price<=bankBalance){
        bankBalance-= price
        noLaptop = false
        alert(`you bought the laptop for ${price} and your balance is now ${bankBalance}`)
        bankBalanceEle.innerText = `Balance: $${bankBalance}`
    }
    else{
        alert('Not enough cash.')
    }
}

const handleLoan = () => {
    const totalPaid = prompt("please enter the amount you wish to loan")
    const change = parseFloat(totalPaid)
    let loanAmount=change;
    if (loanAmount > bankBalance*2){
        alert("Loan refused, insufficient balance.")
    }
    else if (loanBalance>0){
        alert("Loan refused, repay outstanding loan.")
    }
    else if (noLaptop&hasLent){
        alert("Loan refused, insufficient laptops")
    }
    else {
        repayBtnEle.style.visibility = "visible";
        hasLent = true
        bankBalance += loanAmount
        loanBalance += loanAmount
        loanBalanceEle.innerText = `Loan balance: $${loanBalance}`
        bankBalanceEle.innerText = `Bank balance: $${bankBalance}`
    }

}

selectLaptopEle.addEventListener("change", handleLaptopMeneChange)

workBtnEle.addEventListener("click",handleWork)

depositeBtnEle.addEventListener("click",handleDeposit)

buyBtnEle.addEventListener("click",handleBuy)

applyBtnEle.addEventListener("click",handleLoan)

repayBtnEle.addEventListener("click",handleRepay)